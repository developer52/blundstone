function getById(id) {
	return document.getElementById(id)
}
function getByClass(cl) {
	return document.querySelectorAll(cl)
}

document.addEventListener('DOMContentLoaded', function() {
	// search
	(function search() {
		const searchButton = getById('search_button')
		const searchForm = getById('search_form')
		const searchCloseButton = getById('search_close_button')
		const searchInput = getById('search_input')
		searchButton.onclick = function() {
			searchForm.classList.toggle('active')
			searchInput.focus()
			
		}
		searchCloseButton.onclick = function () {
			searchForm.classList.remove('active')
		}
	}());
	// header on scroll - hide announce, logo smaller
	(function headerOnScroll() {
		const header = getById('header')
		if (window.pageYOffset > 40) {
			header.classList.add('scroll')
			
		}
		window.onscroll = function () {
			if (window.pageYOffset > 40) {
				header.classList.add('scroll')
				$("#shopify-section-header").addClass("spacer");
			} else if (window.pageYOffset < 40) {
				header.classList.remove('scroll')
				$("#shopify-section-header").remove("spacer");
			}
		}
	}());
	// window.onscroll = function () {
	// 	// console.log(window.pageYOffset);
	// 	  if (window.pageYOffset > 0) {
	// 		  header.classList.add('scroll')
	// 		  //$("#header-menu").addClass("scroll");
	// 		  $("#shopify-section-header").addClass("spacer");
	// 	  } else if (window.pageYOffset < 110) {
	// 		  header.classList.remove('scroll');
	// 		  //$("#header-menu").removeClass("scroll");
	// 		  $("#shopify-section-header").removeClass("spacer");
	// 	  }
	//   }
	(function mobileMenu() {
		const mobileMenuButton = getById('mobile_menu_button')
		const mobileMenu = getById('mobile_menu')
		const submenuButton = getByClass('.parent-link-mobile')
		mobileMenuButton.onclick = function () {
			mobileMenuButton.classList.toggle('skip-active')
			mobileMenu.classList.toggle('show')
			$(".js-ajax-cart-drawer").removeClass('is-open');
		}
		submenuButton.forEach(function(item) {
			item.onclick = function () {
				if (item.nextSibling) {
					item.nextSibling.classList.toggle('open')
				}
				item.childNodes[1].classList.toggle('plus-minus')
			}
		})
	}());

	// slider init 
	var mySwiper = new Swiper ('.swiper-container', {
		direction: 'horizontal',
		loop: true,
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
			renderBullet: function (index, className) {
			  return '<span class="' + className + '"></span>';
			}
		},
		autoplay: {
			delay: 2500,
			disableOnInteraction: true,
		  },
	
		navigation: {
		  nextEl: '.swiper-button-next',
		  prevEl: '.swiper-button-prev',
		},
	  });
});


document.addEventListener("DOMContentLoaded", function() {
  var endlessScroll = new Ajaxinate({
    container: '#AjaxinateContainer',
    pagination: '#AjaxinatePagination',
    method:'click'
  });
});


 jQuery(document).ready(function() {
	 jQuery('.slider-single').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: true,
	  asNavFor: '.slider-nav'
	});
	jQuery('.slider-nav').slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  asNavFor: '.slider-single',
	  dots: false,
	  centerMode: false,
	  focusOnSelect: true,
	  arrows: true,
	  prevArrow:"<button type='button' class='slick-prev pull-left'><span class='prev'></span></button>",
	  nextArrow:"<button type='button' class='slick-next pull-right'><span class='next'></span></button>",
	});

    setTimeout(function(){ jQuery('.slider-nav').slick('refresh'); }, 1000);

	jQuery('#add').click(function () {
    	jQuery(this).prev().val(+jQuery(this).prev().val() + 1);
	});
	jQuery('#sub').click(function () {
		if (jQuery(this).next().val() > 1) {
    	  if (jQuery(this).next().val() > 1) jQuery(this).next().val(+jQuery(this).next().val() - 1);
		}
	});

	jQuery('.slider-single .slick-current img').elevateZoom();
    jQuery(".slider-single").on("beforeChange", function(
	    event,
	    slick,
	    currentSlide,
	    nextSlide
	  ) {
	    jQuery.removeData(currentSlide, "elevateZoom");
	    jQuery(".zoomContainer").remove();
	  });
	  jQuery(".slider-single").on("afterChange", function() {
	    jQuery(".slider-single .slick-current img").elevateZoom();
	  });


    jQuery('#forgot-password').click(function () {
    	jQuery('#recover-password').show();
    	jQuery('#login_form').hide();
	});

	jQuery('#back-login').click(function () {
    	jQuery('#recover-password').hide();
    	jQuery('#login_form').show();
	});

    jQuery('.stick-social').theiaStickySidebar({
    	  'additionalMarginTop': 100,
	});
	
	jQuery('.faq-tab-box').theiaStickySidebar({
			'additionalMarginTop': 100,
	});

	var filterList = {
		init: function () {
			jQuery('#portfoliolist').mixItUp({
			selectors: {
			  target: '.portfolio',
			  filter: '.filter'	
		  },
		  load: {
  		   filter: 'all'  
  		  }     
		});								
	   }
	};
   filterList.init();
 }); 

 $(document).ready(function(){
	$('.your-class').slick({
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  responsive: [
		{
		  breakpoint: 767,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: true,
			autoplay: false,
		  }
		}
	  ]
	});

   $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	$('.your-class').slick('setPosition');
   });

   $('#content').on('show.bs.collapse', function (e) {
	$('.your-class').slick('refresh');
	$('.your-class').slick('setPosition');
  });

  $('#ContactFormName').focus();


  $('#fax-tab-title li a').click(function() {
	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
	&& location.hostname == this.hostname) {
	  var $target = $(this.hash);
	  $target = $target.length && $target
	  || $('[name=' + this.hash.slice(1) +']');
	  if ($target.length) {
		var targetOffset = $target.offset().top;
		$('html,body')
		.animate({scrollTop: targetOffset -80}, 1200);
	   return false;
	  }
	}
  });

 });
 function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}